var __create = Object.create;
var __defProp = Object.defineProperty;
var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
var __getOwnPropNames = Object.getOwnPropertyNames;
var __getProtoOf = Object.getPrototypeOf, __hasOwnProp = Object.prototype.hasOwnProperty;
var __commonJS = (cb, mod) => function() {
  return mod || (0, cb[__getOwnPropNames(cb)[0]])((mod = { exports: {} }).exports, mod), mod.exports;
};
var __export = (target, all) => {
  for (var name in all)
    __defProp(target, name, { get: all[name], enumerable: !0 });
}, __copyProps = (to, from, except, desc) => {
  if (from && typeof from == "object" || typeof from == "function")
    for (let key of __getOwnPropNames(from))
      !__hasOwnProp.call(to, key) && key !== except && __defProp(to, key, { get: () => from[key], enumerable: !(desc = __getOwnPropDesc(from, key)) || desc.enumerable });
  return to;
};
var __toESM = (mod, isNodeMode, target) => (target = mod != null ? __create(__getProtoOf(mod)) : {}, __copyProps(
  // If the importer is in node compatibility mode or this is not an ESM
  // file that has been converted to a CommonJS file using a Babel-
  // compatible transform (i.e. "__esModule" has not been set), then set
  // "default" to the CommonJS "module.exports" for node compatibility.
  isNodeMode || !mod || !mod.__esModule ? __defProp(target, "default", { value: mod, enumerable: !0 }) : target,
  mod
)), __toCommonJS = (mod) => __copyProps(__defProp({}, "__esModule", { value: !0 }), mod);

// css-bundle-update-plugin-ns:/Users/ezegavilan/Facu/2023/Ing Software/Trabajos Practicos/repos/isw_4k3_g8_2023/material_de_aplicacion/trabajos_practicos/codigo_fuente/ISW_G8_TP4_codigo_fuente_4K3/node_modules/@remix-run/css-bundle/dist/index.js
var require_dist = __commonJS({
  "css-bundle-update-plugin-ns:/Users/ezegavilan/Facu/2023/Ing Software/Trabajos Practicos/repos/isw_4k3_g8_2023/material_de_aplicacion/trabajos_practicos/codigo_fuente/ISW_G8_TP4_codigo_fuente_4K3/node_modules/@remix-run/css-bundle/dist/index.js"(exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: !0 });
    var cssBundleHref2;
    exports.cssBundleHref = cssBundleHref2;
  }
});

// <stdin>
var stdin_exports = {};
__export(stdin_exports, {
  assets: () => assets_manifest_default,
  assetsBuildDirectory: () => assetsBuildDirectory,
  entry: () => entry,
  future: () => future,
  publicPath: () => publicPath,
  routes: () => routes
});
module.exports = __toCommonJS(stdin_exports);

// app/entry.server.tsx
var entry_server_exports = {};
__export(entry_server_exports, {
  default: () => handleRequest
});
var import_node_stream = require("node:stream"), import_node = require("@remix-run/node"), import_react = require("@remix-run/react"), import_isbot = __toESM(require("isbot")), import_server = require("react-dom/server"), import_jsx_dev_runtime = require("react/jsx-dev-runtime"), ABORT_DELAY = 5e3;
function handleRequest(request, responseStatusCode, responseHeaders, remixContext, loadContext) {
  return (0, import_isbot.default)(request.headers.get("user-agent")) ? handleBotRequest(
    request,
    responseStatusCode,
    responseHeaders,
    remixContext
  ) : handleBrowserRequest(
    request,
    responseStatusCode,
    responseHeaders,
    remixContext
  );
}
function handleBotRequest(request, responseStatusCode, responseHeaders, remixContext) {
  return new Promise((resolve, reject) => {
    let shellRendered = !1, { pipe, abort } = (0, import_server.renderToPipeableStream)(
      /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)(
        import_react.RemixServer,
        {
          context: remixContext,
          url: request.url,
          abortDelay: ABORT_DELAY
        },
        void 0,
        !1,
        {
          fileName: "app/entry.server.tsx",
          lineNumber: 48,
          columnNumber: 7
        },
        this
      ),
      {
        onAllReady() {
          shellRendered = !0;
          let body = new import_node_stream.PassThrough();
          responseHeaders.set("Content-Type", "text/html"), resolve(
            new import_node.Response(body, {
              headers: responseHeaders,
              status: responseStatusCode
            })
          ), pipe(body);
        },
        onShellError(error) {
          reject(error);
        },
        onError(error) {
          responseStatusCode = 500, shellRendered && console.error(error);
        }
      }
    );
    setTimeout(abort, ABORT_DELAY);
  });
}
function handleBrowserRequest(request, responseStatusCode, responseHeaders, remixContext) {
  return new Promise((resolve, reject) => {
    let shellRendered = !1, { pipe, abort } = (0, import_server.renderToPipeableStream)(
      /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)(
        import_react.RemixServer,
        {
          context: remixContext,
          url: request.url,
          abortDelay: ABORT_DELAY
        },
        void 0,
        !1,
        {
          fileName: "app/entry.server.tsx",
          lineNumber: 97,
          columnNumber: 7
        },
        this
      ),
      {
        onShellReady() {
          shellRendered = !0;
          let body = new import_node_stream.PassThrough();
          responseHeaders.set("Content-Type", "text/html"), resolve(
            new import_node.Response(body, {
              headers: responseHeaders,
              status: responseStatusCode
            })
          ), pipe(body);
        },
        onShellError(error) {
          reject(error);
        },
        onError(error) {
          responseStatusCode = 500, shellRendered && console.error(error);
        }
      }
    );
    setTimeout(abort, ABORT_DELAY);
  });
}

// app/root.tsx
var root_exports = {};
__export(root_exports, {
  default: () => App,
  links: () => links
});
var import_css_bundle = __toESM(require_dist()), import_react2 = require("@remix-run/react");

// app/styles/tailwind.css
var tailwind_default = "/build/_assets/tailwind-BOQJAR42.css";

// app/styles/index.css
var styles_default = "/build/_assets/index-FXZXIB3F.css";

// app/root.tsx
var import_jsx_dev_runtime2 = require("react/jsx-dev-runtime"), links = () => [
  ...import_css_bundle.cssBundleHref ? [{ rel: "stylesheet", href: import_css_bundle.cssBundleHref }, { rel: "stylesheet", href: tailwind_default }] : [{ rel: "stylesheet", href: tailwind_default }, { rel: "stylesheet", href: styles_default }]
];
function App() {
  return /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)("html", { lang: "en", children: [
    /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)("head", { children: [
      /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)("meta", { charSet: "utf-8" }, void 0, !1, {
        fileName: "app/root.tsx",
        lineNumber: 24,
        columnNumber: 9
      }, this),
      /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)("meta", { name: "viewport", content: "width=device-width,initial-scale=1" }, void 0, !1, {
        fileName: "app/root.tsx",
        lineNumber: 25,
        columnNumber: 9
      }, this),
      /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(import_react2.Meta, {}, void 0, !1, {
        fileName: "app/root.tsx",
        lineNumber: 26,
        columnNumber: 9
      }, this),
      /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(import_react2.Links, {}, void 0, !1, {
        fileName: "app/root.tsx",
        lineNumber: 27,
        columnNumber: 9
      }, this)
    ] }, void 0, !0, {
      fileName: "app/root.tsx",
      lineNumber: 23,
      columnNumber: 7
    }, this),
    /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)("body", { children: [
      /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(import_react2.Outlet, {}, void 0, !1, {
        fileName: "app/root.tsx",
        lineNumber: 30,
        columnNumber: 9
      }, this),
      /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(import_react2.ScrollRestoration, {}, void 0, !1, {
        fileName: "app/root.tsx",
        lineNumber: 31,
        columnNumber: 9
      }, this),
      /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(import_react2.Scripts, {}, void 0, !1, {
        fileName: "app/root.tsx",
        lineNumber: 32,
        columnNumber: 9
      }, this),
      /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(import_react2.LiveReload, {}, void 0, !1, {
        fileName: "app/root.tsx",
        lineNumber: 33,
        columnNumber: 9
      }, this)
    ] }, void 0, !0, {
      fileName: "app/root.tsx",
      lineNumber: 29,
      columnNumber: 7
    }, this)
  ] }, void 0, !0, {
    fileName: "app/root.tsx",
    lineNumber: 22,
    columnNumber: 5
  }, this);
}

// app/routes/checkout.tsx
var checkout_exports = {};
__export(checkout_exports, {
  default: () => CheckoutPage,
  links: () => links2,
  loader: () => loader,
  meta: () => meta
});

// app/styles/checkout.css
var checkout_default = "/build/_assets/checkout-BIRQVL6Q.css";

// app/routes/checkout.tsx
var import_react3 = require("react"), import_react4 = require("@remix-run/react"), import_react_router_dom = require("react-router-dom"), import_jsx_dev_runtime3 = require("react/jsx-dev-runtime"), meta = () => ({ title: "Complet\xE1 tu pedido" }), links2 = () => [{ rel: "stylesheet", href: checkout_default }], loader = ({ params }) => params;
function CheckoutPage() {
  let items = [
    { id: 1, name: "Burger Premium", img: "/img/burger-1.png", price: 3500 }
  ], [itemsState, setItemsState] = (0, import_react3.useState)(items), matches = (0, import_react4.useMatches)(), activeRoute = matches[matches.length - 1].pathname, isCheckoutRootPage = activeRoute === "/checkout" || activeRoute === "/checkout/", [cta, setCta] = (0, import_react3.useState)(isCheckoutRootPage), handleRemoveItem = (id) => {
    setItemsState(itemsState.filter((item) => item.id !== id));
  }, handleReturnToCheckout = () => {
    setItemsState(items);
  };
  return /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)("main", { className: "container m-5 px-5", children: [
    /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)("section", { className: "title", children: [
      /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)("h1", { className: " text-xl font-bold", children: "Complet\xE1 tu pedido" }, void 0, !1, {
        fileName: "app/routes/checkout.tsx",
        lineNumber: 41,
        columnNumber: 17
      }, this),
      /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)("span", { className: "text-sm", children: /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)("p", { className: "", children: "| One Burger" }, void 0, !1, {
        fileName: "app/routes/checkout.tsx",
        lineNumber: 43,
        columnNumber: 21
      }, this) }, void 0, !1, {
        fileName: "app/routes/checkout.tsx",
        lineNumber: 42,
        columnNumber: 17
      }, this)
    ] }, void 0, !0, {
      fileName: "app/routes/checkout.tsx",
      lineNumber: 40,
      columnNumber: 13
    }, this),
    /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)("section", { className: "summary mt-12", children: [
      itemsState.map(
        (item) => /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)("div", { className: "summary-item", children: [
          /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)("span", { className: "item-description", children: [
            /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)("span", { className: "img", children: /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)("img", { src: item.img, alt: item.name }, void 0, !1, {
              fileName: "app/routes/checkout.tsx",
              lineNumber: 53,
              columnNumber: 37
            }, this) }, void 0, !1, {
              fileName: "app/routes/checkout.tsx",
              lineNumber: 52,
              columnNumber: 33
            }, this),
            /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)("div", { className: "item-name", children: [
              /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)("p", { children: item.name }, void 0, !1, {
                fileName: "app/routes/checkout.tsx",
                lineNumber: 56,
                columnNumber: 37
              }, this),
              /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)("p", { className: "units", children: "x1" }, void 0, !1, {
                fileName: "app/routes/checkout.tsx",
                lineNumber: 57,
                columnNumber: 37
              }, this)
            ] }, void 0, !0, {
              fileName: "app/routes/checkout.tsx",
              lineNumber: 55,
              columnNumber: 33
            }, this)
          ] }, void 0, !0, {
            fileName: "app/routes/checkout.tsx",
            lineNumber: 51,
            columnNumber: 29
          }, this),
          /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)("div", { className: "price-remove", children: [
            /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)("span", { children: /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)("p", { children: [
              "$ ",
              item.price
            ] }, void 0, !0, {
              fileName: "app/routes/checkout.tsx",
              lineNumber: 61,
              columnNumber: 39
            }, this) }, void 0, !1, {
              fileName: "app/routes/checkout.tsx",
              lineNumber: 61,
              columnNumber: 33
            }, this),
            cta && /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)("button", { className: "remove", onClick: () => handleRemoveItem(item.id), children: /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)("span", { children: /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)("svg", { xmlns: "http://www.w3.org/2000/svg", fill: "none", viewBox: "0 0 24 24", "stroke-width": "1.5", stroke: "currentColor", className: "w-6 h-6", children: /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)("path", { "stroke-linecap": "round", "stroke-linejoin": "round", d: "M20.25 7.5l-.625 10.632a2.25 2.25 0 01-2.247 2.118H6.622a2.25 2.25 0 01-2.247-2.118L3.75 7.5m6 4.125l2.25 2.25m0 0l2.25 2.25M12 13.875l2.25-2.25M12 13.875l-2.25 2.25M3.375 7.5h17.25c.621 0 1.125-.504 1.125-1.125v-1.5c0-.621-.504-1.125-1.125-1.125H3.375c-.621 0-1.125.504-1.125 1.125v1.5c0 .621.504 1.125 1.125 1.125z" }, void 0, !1, {
              fileName: "app/routes/checkout.tsx",
              lineNumber: 66,
              columnNumber: 49
            }, this) }, void 0, !1, {
              fileName: "app/routes/checkout.tsx",
              lineNumber: 65,
              columnNumber: 51
            }, this) }, void 0, !1, {
              fileName: "app/routes/checkout.tsx",
              lineNumber: 65,
              columnNumber: 45
            }, this) }, void 0, !1, {
              fileName: "app/routes/checkout.tsx",
              lineNumber: 64,
              columnNumber: 13
            }, this)
          ] }, void 0, !0, {
            fileName: "app/routes/checkout.tsx",
            lineNumber: 60,
            columnNumber: 29
          }, this)
        ] }, item.name, !0, {
          fileName: "app/routes/checkout.tsx",
          lineNumber: 50,
          columnNumber: 9
        }, this)
      ),
      cta && itemsState.length > 0 && /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)("div", { className: "mt-7 p-2", children: /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)(import_react_router_dom.Link, { className: "cta", to: "steps/step-1", onClick: () => setCta(!1), children: /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)("span", { children: "Continuar" }, void 0, !1, {
        fileName: "app/routes/checkout.tsx",
        lineNumber: 81,
        columnNumber: 33
      }, this) }, void 0, !1, {
        fileName: "app/routes/checkout.tsx",
        lineNumber: 80,
        columnNumber: 29
      }, this) }, void 0, !1, {
        fileName: "app/routes/checkout.tsx",
        lineNumber: 79,
        columnNumber: 9
      }, this),
      itemsState.length <= 0 && /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)("div", { className: "empty-items", children: [
        /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)("h1", { children: "\xA1Agreg\xE1 productos al carrito para continuar!" }, void 0, !1, {
          fileName: "app/routes/checkout.tsx",
          lineNumber: 91,
          columnNumber: 29
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)("button", { className: "cta sm", onClick: handleReturnToCheckout, children: "Volver" }, void 0, !1, {
          fileName: "app/routes/checkout.tsx",
          lineNumber: 92,
          columnNumber: 29
        }, this)
      ] }, void 0, !0, {
        fileName: "app/routes/checkout.tsx",
        lineNumber: 90,
        columnNumber: 9
      }, this)
    ] }, void 0, !0, {
      fileName: "app/routes/checkout.tsx",
      lineNumber: 47,
      columnNumber: 13
    }, this),
    /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)("section", { className: "location-container mt-5", children: /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)(import_react4.Outlet, {}, void 0, !1, {
      fileName: "app/routes/checkout.tsx",
      lineNumber: 99,
      columnNumber: 17
    }, this) }, void 0, !1, {
      fileName: "app/routes/checkout.tsx",
      lineNumber: 98,
      columnNumber: 13
    }, this)
  ] }, void 0, !0, {
    fileName: "app/routes/checkout.tsx",
    lineNumber: 39,
    columnNumber: 5
  }, this);
}

// app/routes/checkout/steps.tsx
var steps_exports = {};
__export(steps_exports, {
  default: () => CheckoutStepsPage
});
var import_react5 = require("@material-tailwind/react"), import_react6 = require("@remix-run/react"), import_jsx_dev_runtime4 = require("react/jsx-dev-runtime");
function CheckoutStepsPage() {
  let matches = (0, import_react6.useMatches)(), activeStep = matches[matches.length - 1].pathname.split("/")[matches.length - 1].split("-")[1];
  return /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)(import_jsx_dev_runtime4.Fragment, { children: /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)(StepperWithContent, { step: Number(activeStep) }, void 0, !1, {
    fileName: "app/routes/checkout/steps.tsx",
    lineNumber: 11,
    columnNumber: 13
  }, this) }, void 0, !1, {
    fileName: "app/routes/checkout/steps.tsx",
    lineNumber: 10,
    columnNumber: 9
  }, this);
}
function StepperWithContent({ step }) {
  return /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("div", { className: "w-full px-24 py-4", children: [
    /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)(
      import_react5.Stepper,
      {
        activeLineClassName: "!bg-delivereat-secondary",
        activeStep: step - 1,
        children: [
          /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)(import_react5.Step, { activeClassName: "!bg-delivereat-primary", completedClassName: "!bg-delivereat-secondary", children: [
            "1",
            /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("div", { className: "absolute -bottom-[4.5rem] w-max text-center" }, void 0, !1, {
              fileName: "app/routes/checkout/steps.tsx",
              lineNumber: 28,
              columnNumber: 21
            }, this)
          ] }, void 0, !0, {
            fileName: "app/routes/checkout/steps.tsx",
            lineNumber: 26,
            columnNumber: 17
          }, this),
          /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)(import_react5.Step, { activeClassName: "!bg-delivereat-primary", completedClassName: "!bg-delivereat-secondary", children: [
            "2",
            /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("div", { className: "absolute -bottom-[4.5rem] w-max text-center" }, void 0, !1, {
              fileName: "app/routes/checkout/steps.tsx",
              lineNumber: 34,
              columnNumber: 21
            }, this)
          ] }, void 0, !0, {
            fileName: "app/routes/checkout/steps.tsx",
            lineNumber: 32,
            columnNumber: 17
          }, this),
          /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)(import_react5.Step, { activeClassName: "!bg-delivereat-primary", completedClassName: "!bg-delivereat-secondary", children: [
            "3",
            /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("div", { className: "absolute -bottom-[4.5rem] w-max text-center" }, void 0, !1, {
              fileName: "app/routes/checkout/steps.tsx",
              lineNumber: 40,
              columnNumber: 21
            }, this)
          ] }, void 0, !0, {
            fileName: "app/routes/checkout/steps.tsx",
            lineNumber: 38,
            columnNumber: 17
          }, this)
        ]
      },
      void 0,
      !0,
      {
        fileName: "app/routes/checkout/steps.tsx",
        lineNumber: 23,
        columnNumber: 13
      },
      this
    ),
    /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)(import_react6.Outlet, {}, void 0, !1, {
      fileName: "app/routes/checkout/steps.tsx",
      lineNumber: 45,
      columnNumber: 13
    }, this)
  ] }, void 0, !0, {
    fileName: "app/routes/checkout/steps.tsx",
    lineNumber: 22,
    columnNumber: 9
  }, this);
}

// app/routes/checkout/steps/step-1.tsx
var step_1_exports = {};
__export(step_1_exports, {
  action: () => action,
  default: () => LocationStepPage
});
var import_node2 = require("@remix-run/node"), import_react7 = require("@remix-run/react");

// app/validations/location.server.ts
function isValidStreet(value) {
  return value && value.trim().length > 0;
}
function isValidStreetNumber(value) {
  return !isNaN(value) && value > 0;
}
function isValidCity(value) {
  return [
    "Villa Carlos Paz",
    "Santa Rosa de Calamuchita",
    "Villa General Belgrano",
    "Cosquin",
    "Mina Clavero",
    "Nono"
  ].includes(value);
}
function validateLocationInput(input) {
  let validationErrors = {};
  if (isValidStreet(input.street) || (validationErrors.street = "Ingres\xE1 el nombre de tu calle."), isValidStreetNumber(input["street-number"]) || (validationErrors["street-number"] = "Ingres\xE1 el n\xFAmero de tu calle."), isValidCity(input.city) || (validationErrors.city = "Seleccion\xE1 tu ciudad."), Object.keys(validationErrors).length > 0)
    throw validationErrors;
}

// app/routes/checkout/steps/step-1.tsx
var import_jsx_dev_runtime5 = require("react/jsx-dev-runtime"), action = async ({ request }) => {
  let formData = await request.formData(), location = Object.fromEntries(formData);
  try {
    validateLocationInput(location);
  } catch (error) {
    return console.error(error), error;
  }
  return (0, import_node2.redirect)("/checkout/steps/step-2");
};
function LocationStepPage() {
  let navigate = (0, import_react7.useNavigate)(), validationErrors = (0, import_react7.useActionData)(), handlePrev = (e) => {
    e.preventDefault(), navigate("/checkout", { replace: !1 }), window.location.reload();
  };
  return /* @__PURE__ */ (0, import_jsx_dev_runtime5.jsxDEV)(import_react7.Form, { method: "POST", className: "mt-10 location", children: [
    /* @__PURE__ */ (0, import_jsx_dev_runtime5.jsxDEV)("div", { className: "relative", children: [
      /* @__PURE__ */ (0, import_jsx_dev_runtime5.jsxDEV)("div", { className: "absolute inset-y-0 left-0 flex items-center pl-3.5 pointer-events-none", children: /* @__PURE__ */ (0, import_jsx_dev_runtime5.jsxDEV)("svg", { xmlns: "http://www.w3.org/2000/svg", fill: "none", viewBox: "0 0 24 24", "stroke-width": "1.5", stroke: "currentColor", className: "w-6 h-6", children: [
        /* @__PURE__ */ (0, import_jsx_dev_runtime5.jsxDEV)("path", { "stroke-linecap": "round", "stroke-linejoin": "round", d: "M15 10.5a3 3 0 11-6 0 3 3 0 016 0z" }, void 0, !1, {
          fileName: "app/routes/checkout/steps/step-1.tsx",
          lineNumber: 35,
          columnNumber: 25
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime5.jsxDEV)("path", { "stroke-linecap": "round", "stroke-linejoin": "round", d: "M19.5 10.5c0 7.142-7.5 11.25-7.5 11.25S4.5 17.642 4.5 10.5a7.5 7.5 0 1115 0z" }, void 0, !1, {
          fileName: "app/routes/checkout/steps/step-1.tsx",
          lineNumber: 36,
          columnNumber: 25
        }, this)
      ] }, void 0, !0, {
        fileName: "app/routes/checkout/steps/step-1.tsx",
        lineNumber: 34,
        columnNumber: 21
      }, this) }, void 0, !1, {
        fileName: "app/routes/checkout/steps/step-1.tsx",
        lineNumber: 33,
        columnNumber: 17
      }, this),
      /* @__PURE__ */ (0, import_jsx_dev_runtime5.jsxDEV)("select", { name: "city", defaultValue: "default", id: "email-address-icon", className: "bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500", placeholder: "Seleccion\xE1 tu ciudad", required: !0, children: [
        /* @__PURE__ */ (0, import_jsx_dev_runtime5.jsxDEV)("option", { value: "default", children: "Seleccion\xE1 tu ciudad" }, void 0, !1, {
          fileName: "app/routes/checkout/steps/step-1.tsx",
          lineNumber: 41,
          columnNumber: 21
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime5.jsxDEV)("option", { value: "Villa Carlos Paz", children: "Villa Carlos Paz" }, void 0, !1, {
          fileName: "app/routes/checkout/steps/step-1.tsx",
          lineNumber: 42,
          columnNumber: 21
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime5.jsxDEV)("option", { value: "Santa Rosa de Calamuchita", children: "Santa Rosa de Calamuchita" }, void 0, !1, {
          fileName: "app/routes/checkout/steps/step-1.tsx",
          lineNumber: 43,
          columnNumber: 21
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime5.jsxDEV)("option", { value: "Villa General Belgrano", children: "Villa General Belgrano" }, void 0, !1, {
          fileName: "app/routes/checkout/steps/step-1.tsx",
          lineNumber: 44,
          columnNumber: 21
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime5.jsxDEV)("option", { value: "Cosquin", children: "Cosquin" }, void 0, !1, {
          fileName: "app/routes/checkout/steps/step-1.tsx",
          lineNumber: 45,
          columnNumber: 21
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime5.jsxDEV)("option", { value: "Mina Clavero", children: "Mina Clavero" }, void 0, !1, {
          fileName: "app/routes/checkout/steps/step-1.tsx",
          lineNumber: 46,
          columnNumber: 21
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime5.jsxDEV)("option", { value: "Nono", children: "Nono" }, void 0, !1, {
          fileName: "app/routes/checkout/steps/step-1.tsx",
          lineNumber: 47,
          columnNumber: 21
        }, this)
      ] }, void 0, !0, {
        fileName: "app/routes/checkout/steps/step-1.tsx",
        lineNumber: 40,
        columnNumber: 17
      }, this)
    ] }, void 0, !0, {
      fileName: "app/routes/checkout/steps/step-1.tsx",
      lineNumber: 32,
      columnNumber: 13
    }, this),
    /* @__PURE__ */ (0, import_jsx_dev_runtime5.jsxDEV)("div", { className: "mt-5 columns-2", children: [
      /* @__PURE__ */ (0, import_jsx_dev_runtime5.jsxDEV)("input", { type: "text", id: "street", name: "street", className: "bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500", placeholder: "Calle", required: !0 }, void 0, !1, {
        fileName: "app/routes/checkout/steps/step-1.tsx",
        lineNumber: 52,
        columnNumber: 17
      }, this),
      /* @__PURE__ */ (0, import_jsx_dev_runtime5.jsxDEV)("input", { type: "number", id: "street-number", name: "street-number", className: "bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-2/5 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500", placeholder: "N\xFAmero", required: !0 }, void 0, !1, {
        fileName: "app/routes/checkout/steps/step-1.tsx",
        lineNumber: 54,
        columnNumber: 17
      }, this)
    ] }, void 0, !0, {
      fileName: "app/routes/checkout/steps/step-1.tsx",
      lineNumber: 51,
      columnNumber: 13
    }, this),
    /* @__PURE__ */ (0, import_jsx_dev_runtime5.jsxDEV)("div", { className: "mt-5 columns-2", children: /* @__PURE__ */ (0, import_jsx_dev_runtime5.jsxDEV)("input", { type: "text", id: "ref", name: "ref", className: "bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500", placeholder: "Referencia para cadete" }, void 0, !1, {
      fileName: "app/routes/checkout/steps/step-1.tsx",
      lineNumber: 58,
      columnNumber: 17
    }, this) }, void 0, !1, {
      fileName: "app/routes/checkout/steps/step-1.tsx",
      lineNumber: 57,
      columnNumber: 13
    }, this),
    validationErrors && /* @__PURE__ */ (0, import_jsx_dev_runtime5.jsxDEV)("ul", { className: "mt-5", children: Object.values(validationErrors).map((error) => /* @__PURE__ */ (0, import_jsx_dev_runtime5.jsxDEV)("li", { className: "text-xs text-red-300", children: error }, error, !1, {
      fileName: "app/routes/checkout/steps/step-1.tsx",
      lineNumber: 66,
      columnNumber: 33
    }, this)) }, void 0, !1, {
      fileName: "app/routes/checkout/steps/step-1.tsx",
      lineNumber: 63,
      columnNumber: 21
    }, this),
    /* @__PURE__ */ (0, import_jsx_dev_runtime5.jsxDEV)("div", { className: "mt-16 flex justify-between", children: [
      /* @__PURE__ */ (0, import_jsx_dev_runtime5.jsxDEV)("div", { className: "mt-16 mx-3", children: /* @__PURE__ */ (0, import_jsx_dev_runtime5.jsxDEV)("button", { className: "cta sm", onClick: handlePrev, children: "Anterior" }, void 0, !1, {
        fileName: "app/routes/checkout/steps/step-1.tsx",
        lineNumber: 76,
        columnNumber: 21
      }, this) }, void 0, !1, {
        fileName: "app/routes/checkout/steps/step-1.tsx",
        lineNumber: 75,
        columnNumber: 17
      }, this),
      /* @__PURE__ */ (0, import_jsx_dev_runtime5.jsxDEV)("div", { className: "mt-16 flex justify-end", children: /* @__PURE__ */ (0, import_jsx_dev_runtime5.jsxDEV)("button", { className: "cta sm", children: "Siguiente" }, void 0, !1, {
        fileName: "app/routes/checkout/steps/step-1.tsx",
        lineNumber: 82,
        columnNumber: 21
      }, this) }, void 0, !1, {
        fileName: "app/routes/checkout/steps/step-1.tsx",
        lineNumber: 81,
        columnNumber: 17
      }, this)
    ] }, void 0, !0, {
      fileName: "app/routes/checkout/steps/step-1.tsx",
      lineNumber: 73,
      columnNumber: 13
    }, this)
  ] }, void 0, !0, {
    fileName: "app/routes/checkout/steps/step-1.tsx",
    lineNumber: 31,
    columnNumber: 9
  }, this);
}

// app/routes/checkout/steps/step-2.tsx
var step_2_exports = {};
__export(step_2_exports, {
  action: () => action2,
  default: () => DeliveryTimeStepPage
});
var import_node3 = require("@remix-run/node"), import_react8 = require("@remix-run/react"), import_react9 = require("react");

// app/validations/delivery.server.ts
function isValidDeliveryTime(value) {
  if (!value)
    return !0;
  let currentDate = /* @__PURE__ */ new Date(), days = (new Date(value).getTime() - currentDate.getTime()) / (1e3 * 60 * 60 * 24);
  return days <= 7 && days > 0;
}
function validateDeliveryTimeInput(input) {
  let validationErrors = {};
  if (isValidDeliveryTime(input.deliveryTime) || (validationErrors.deliveryTime = "La fecha debe ser entre hoy y 7 d\xEDas."), Object.keys(validationErrors).length > 0)
    throw validationErrors;
}

// app/routes/checkout/steps/step-2.tsx
var import_jsx_dev_runtime6 = require("react/jsx-dev-runtime"), action2 = async ({ request }) => {
  let formData = await request.formData(), deliveryTime = Object.fromEntries(formData);
  if (deliveryTime.deliveryTime === "skip")
    return (0, import_node3.redirect)("/checkout/steps/step-3");
  try {
    console.log({ deliveryTime }), validateDeliveryTimeInput(deliveryTime);
  } catch (error) {
    return console.error(error), error;
  }
  return (0, import_node3.redirect)("/checkout/steps/step-3");
};
function DeliveryTimeStepPage() {
  let navigate = (0, import_react8.useNavigate)(), validationErrors = (0, import_react8.useActionData)(), [selected, setSelected] = (0, import_react9.useState)("sooner"), handleSelectCard = (card) => {
    setSelected(card);
  }, handlePrev = (e) => {
    e.preventDefault(), navigate("/checkout/steps/step-1");
  };
  return /* @__PURE__ */ (0, import_jsx_dev_runtime6.jsxDEV)(import_react8.Form, { method: "POST", children: [
    /* @__PURE__ */ (0, import_jsx_dev_runtime6.jsxDEV)("div", { className: "delivery-time mt-5", children: [
      /* @__PURE__ */ (0, import_jsx_dev_runtime6.jsxDEV)("div", { className: `card ${selected === "sooner" ? "selected" : ""}`, onClick: () => handleSelectCard("sooner"), children: /* @__PURE__ */ (0, import_jsx_dev_runtime6.jsxDEV)("span", { className: "text-2xl", children: /* @__PURE__ */ (0, import_jsx_dev_runtime6.jsxDEV)("p", { children: "\xA1Lo antes posible!" }, void 0, !1, {
        fileName: "app/routes/checkout/steps/step-2.tsx",
        lineNumber: 42,
        columnNumber: 48
      }, this) }, void 0, !1, {
        fileName: "app/routes/checkout/steps/step-2.tsx",
        lineNumber: 42,
        columnNumber: 21
      }, this) }, void 0, !1, {
        fileName: "app/routes/checkout/steps/step-2.tsx",
        lineNumber: 41,
        columnNumber: 17
      }, this),
      /* @__PURE__ */ (0, import_jsx_dev_runtime6.jsxDEV)("div", { className: `card ${selected !== "sooner" ? "selected" : ""}`, onClick: () => handleSelectCard("later"), children: [
        selected !== "later" && /* @__PURE__ */ (0, import_jsx_dev_runtime6.jsxDEV)(import_jsx_dev_runtime6.Fragment, { children: [
          /* @__PURE__ */ (0, import_jsx_dev_runtime6.jsxDEV)("span", { className: "text-2xl", children: /* @__PURE__ */ (0, import_jsx_dev_runtime6.jsxDEV)("p", { children: "Quiero elegir cuando" }, void 0, !1, {
            fileName: "app/routes/checkout/steps/step-2.tsx",
            lineNumber: 49,
            columnNumber: 60
          }, this) }, void 0, !1, {
            fileName: "app/routes/checkout/steps/step-2.tsx",
            lineNumber: 49,
            columnNumber: 33
          }, this),
          /* @__PURE__ */ (0, import_jsx_dev_runtime6.jsxDEV)("input", { defaultValue: "skip", type: "text", name: "deliveryTime", hidden: !0 }, void 0, !1, {
            fileName: "app/routes/checkout/steps/step-2.tsx",
            lineNumber: 50,
            columnNumber: 33
          }, this)
        ] }, void 0, !0, {
          fileName: "app/routes/checkout/steps/step-2.tsx",
          lineNumber: 48,
          columnNumber: 29
        }, this),
        selected === "later" && /* @__PURE__ */ (0, import_jsx_dev_runtime6.jsxDEV)("div", { className: "schedule-order", children: [
          /* @__PURE__ */ (0, import_jsx_dev_runtime6.jsxDEV)("span", { className: "text-2xl", children: /* @__PURE__ */ (0, import_jsx_dev_runtime6.jsxDEV)("p", { children: "\xBFCuando lo quer\xE9s?" }, void 0, !1, {
            fileName: "app/routes/checkout/steps/step-2.tsx",
            lineNumber: 58,
            columnNumber: 60
          }, this) }, void 0, !1, {
            fileName: "app/routes/checkout/steps/step-2.tsx",
            lineNumber: 58,
            columnNumber: 33
          }, this),
          /* @__PURE__ */ (0, import_jsx_dev_runtime6.jsxDEV)("input", { name: "deliveryTime", type: "datetime-local", id: "deliveryTime", className: "bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500", placeholder: "\xBFCuando lo quer\xE9s?", required: !0 }, void 0, !1, {
            fileName: "app/routes/checkout/steps/step-2.tsx",
            lineNumber: 59,
            columnNumber: 33
          }, this)
        ] }, void 0, !0, {
          fileName: "app/routes/checkout/steps/step-2.tsx",
          lineNumber: 57,
          columnNumber: 29
        }, this)
      ] }, void 0, !0, {
        fileName: "app/routes/checkout/steps/step-2.tsx",
        lineNumber: 45,
        columnNumber: 17
      }, this)
    ] }, void 0, !0, {
      fileName: "app/routes/checkout/steps/step-2.tsx",
      lineNumber: 40,
      columnNumber: 13
    }, this),
    validationErrors && /* @__PURE__ */ (0, import_jsx_dev_runtime6.jsxDEV)("ul", { className: "mt-5", children: Object.values(validationErrors).map((error) => /* @__PURE__ */ (0, import_jsx_dev_runtime6.jsxDEV)("li", { className: "text-xs text-red-300", children: error }, error, !1, {
      fileName: "app/routes/checkout/steps/step-2.tsx",
      lineNumber: 71,
      columnNumber: 33
    }, this)) }, void 0, !1, {
      fileName: "app/routes/checkout/steps/step-2.tsx",
      lineNumber: 68,
      columnNumber: 21
    }, this),
    /* @__PURE__ */ (0, import_jsx_dev_runtime6.jsxDEV)("div", { className: "mt-16 flex justify-between", children: [
      /* @__PURE__ */ (0, import_jsx_dev_runtime6.jsxDEV)("div", { className: "mt-16 mx-3", children: /* @__PURE__ */ (0, import_jsx_dev_runtime6.jsxDEV)("button", { className: "cta sm", onClick: handlePrev, children: "Anterior" }, void 0, !1, {
        fileName: "app/routes/checkout/steps/step-2.tsx",
        lineNumber: 81,
        columnNumber: 21
      }, this) }, void 0, !1, {
        fileName: "app/routes/checkout/steps/step-2.tsx",
        lineNumber: 80,
        columnNumber: 17
      }, this),
      /* @__PURE__ */ (0, import_jsx_dev_runtime6.jsxDEV)("div", { className: "mt-16 flex justify-end", children: /* @__PURE__ */ (0, import_jsx_dev_runtime6.jsxDEV)("button", { className: "cta sm", children: "Siguiente" }, void 0, !1, {
        fileName: "app/routes/checkout/steps/step-2.tsx",
        lineNumber: 87,
        columnNumber: 21
      }, this) }, void 0, !1, {
        fileName: "app/routes/checkout/steps/step-2.tsx",
        lineNumber: 86,
        columnNumber: 17
      }, this)
    ] }, void 0, !0, {
      fileName: "app/routes/checkout/steps/step-2.tsx",
      lineNumber: 78,
      columnNumber: 13
    }, this)
  ] }, void 0, !0, {
    fileName: "app/routes/checkout/steps/step-2.tsx",
    lineNumber: 39,
    columnNumber: 9
  }, this);
}

// app/routes/checkout/steps/step-3.tsx
var step_3_exports = {};
__export(step_3_exports, {
  action: () => action3,
  default: () => PayMethodStep
});
var import_node4 = require("@remix-run/node"), import_react10 = require("@remix-run/react"), import_react11 = require("react");

// app/validations/payment.server.ts
function isValidAmount(value) {
  let amount = Number(value);
  return amount > 0 && amount <= 1e5 && value !== "";
}
function isValidDifferenceAmount(value) {
  return Number(value) >= 6e3;
}
function validateCashPaymentInput(amount) {
  let validationErrors = {};
  if (isValidAmount(amount) || (validationErrors.amount = "Pod\xE9s pagar desde $ 0 hasta $ 100.000."), isValidDifferenceAmount(amount) || (validationErrors.amountDiff = "Necesit\xE1s cubrir el total del pedido."), Object.keys(validationErrors).length > 0)
    throw validationErrors;
}
function isValidCardNumber(value) {
  return value.length === 16 + 3 && value.startsWith("4");
}
function isValidExpired(value) {
  let month = Number(value.split("/")[0]), year = Number(value.split("/")[1]) + 2e3;
  if (month > 12 || month <= 0)
    return !1;
  let currentDate = /* @__PURE__ */ new Date(), vtoDate = new Date(year, month - 1, 1);
  return currentDate.getTime() <= vtoDate.getTime();
}
function validateCreditCardInput(input) {
  let validationErrors = {};
  if (isValidCardNumber(input.card) || (validationErrors.creditCard = "Datos de tarjeta visa inv\xE1lidos."), isValidExpired(input.vto) || (validationErrors.expires = "Revis\xE1 el vencimiento de tu tarjeta."), Object.keys(validationErrors).length > 0)
    throw validationErrors;
}

// app/routes/checkout/steps/step-3.tsx
var import_jsx_dev_runtime7 = require("react/jsx-dev-runtime"), action3 = async ({ request }) => {
  let formData = await request.formData(), cashAmount = formData.get("amount");
  if (cashAmount || cashAmount === "")
    try {
      return validateCashPaymentInput(cashAmount), (0, import_node4.redirect)("/success");
    } catch (error) {
      return console.error(error), error;
    }
  try {
    let creditCardData = Object.fromEntries(formData);
    return validateCreditCardInput(creditCardData), (0, import_node4.redirect)("/success");
  } catch (error) {
    return console.error(error), error;
  }
};
function PayMethodStep({ total = 6e3 }) {
  let navigate = (0, import_react10.useNavigate)(), validationErrors = (0, import_react10.useActionData)(), [selected, setSelected] = (0, import_react11.useState)("cash"), [amount, setAmount] = (0, import_react11.useState)(0), [cardValue, setCardValue] = (0, import_react11.useState)(""), [vtoValue, setVtoValue] = (0, import_react11.useState)(""), handleCardValueChange = (e) => {
    let rawValue = e.target.value.replace(/\D/g, ""), formattedValue = "";
    for (let i = 0; i < rawValue.length; i++)
      i > 0 && i % 4 === 0 && (formattedValue += "-"), formattedValue += rawValue[i];
    setCardValue(formattedValue);
  }, handleVtoValueChange = (e) => {
    let rawValue = e.target.value.replace(/\D/g, ""), formattedValue = "";
    for (let i = 0; i < rawValue.length; i++)
      i > 0 && i % 2 === 0 && (formattedValue += "/"), formattedValue += rawValue[i];
    setVtoValue(formattedValue);
  }, handleSelectCard = (card) => {
    setSelected(card);
  }, handleChangeAmount = (e) => {
    setAmount(Number(e.target.value));
  }, calculateExchange = (amount2) => amount2 - total, handlePrev = (e) => {
    e.preventDefault(), navigate("/checkout/steps/step-2");
  };
  return /* @__PURE__ */ (0, import_jsx_dev_runtime7.jsxDEV)(import_react10.Form, { method: "POST", children: [
    /* @__PURE__ */ (0, import_jsx_dev_runtime7.jsxDEV)("div", { className: "payment-container mt-10", children: [
      /* @__PURE__ */ (0, import_jsx_dev_runtime7.jsxDEV)("div", { className: "payment mx-5", children: /* @__PURE__ */ (0, import_jsx_dev_runtime7.jsxDEV)("div", { className: "payment-method", children: [
        /* @__PURE__ */ (0, import_jsx_dev_runtime7.jsxDEV)("span", { children: /* @__PURE__ */ (0, import_jsx_dev_runtime7.jsxDEV)("p", { children: "\xBFC\xF3mo vas a pagar?" }, void 0, !1, {
          fileName: "app/routes/checkout/steps/step-3.tsx",
          lineNumber: 88,
          columnNumber: 29
        }, this) }, void 0, !1, {
          fileName: "app/routes/checkout/steps/step-3.tsx",
          lineNumber: 87,
          columnNumber: 25
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime7.jsxDEV)("div", { className: "methods-cards-container mt-5", children: [
          /* @__PURE__ */ (0, import_jsx_dev_runtime7.jsxDEV)("span", { className: `method-card ${selected === "cash" ? "selected" : ""}`, onClick: () => handleSelectCard("cash"), children: [
            /* @__PURE__ */ (0, import_jsx_dev_runtime7.jsxDEV)("span", { className: "mx-1", children: /* @__PURE__ */ (0, import_jsx_dev_runtime7.jsxDEV)("svg", { xmlns: "http://www.w3.org/2000/svg", fill: "none", viewBox: "0 0 24 24", "stroke-width": "1.5", stroke: "currentColor", className: "w-6 h-6", children: /* @__PURE__ */ (0, import_jsx_dev_runtime7.jsxDEV)("path", { "stroke-linecap": "round", "stroke-linejoin": "round", d: "M2.25 18.75a60.07 60.07 0 0115.797 2.101c.727.198 1.453-.342 1.453-1.096V18.75M3.75 4.5v.75A.75.75 0 013 6h-.75m0 0v-.375c0-.621.504-1.125 1.125-1.125H20.25M2.25 6v9m18-10.5v.75c0 .414.336.75.75.75h.75m-1.5-1.5h.375c.621 0 1.125.504 1.125 1.125v9.75c0 .621-.504 1.125-1.125 1.125h-.375m1.5-1.5H21a.75.75 0 00-.75.75v.75m0 0H3.75m0 0h-.375a1.125 1.125 0 01-1.125-1.125V15m1.5 1.5v-.75A.75.75 0 003 15h-.75M15 10.5a3 3 0 11-6 0 3 3 0 016 0zm3 0h.008v.008H18V10.5zm-12 0h.008v.008H6V10.5z" }, void 0, !1, {
              fileName: "app/routes/checkout/steps/step-3.tsx",
              lineNumber: 95,
              columnNumber: 41
            }, this) }, void 0, !1, {
              fileName: "app/routes/checkout/steps/step-3.tsx",
              lineNumber: 94,
              columnNumber: 37
            }, this) }, void 0, !1, {
              fileName: "app/routes/checkout/steps/step-3.tsx",
              lineNumber: 93,
              columnNumber: 33
            }, this),
            /* @__PURE__ */ (0, import_jsx_dev_runtime7.jsxDEV)("p", { children: "CASH" }, void 0, !1, {
              fileName: "app/routes/checkout/steps/step-3.tsx",
              lineNumber: 98,
              columnNumber: 33
            }, this)
          ] }, void 0, !0, {
            fileName: "app/routes/checkout/steps/step-3.tsx",
            lineNumber: 92,
            columnNumber: 29
          }, this),
          /* @__PURE__ */ (0, import_jsx_dev_runtime7.jsxDEV)("span", { className: `method-card ${selected === "visa" ? "selected" : ""}`, onClick: () => handleSelectCard("visa"), children: [
            /* @__PURE__ */ (0, import_jsx_dev_runtime7.jsxDEV)("span", { className: "mx-1", children: /* @__PURE__ */ (0, import_jsx_dev_runtime7.jsxDEV)("svg", { xmlns: "http://www.w3.org/2000/svg", fill: "none", viewBox: "0 0 24 24", strokeWidth: 1.5, stroke: "currentColor", className: "w-6 h-6", children: /* @__PURE__ */ (0, import_jsx_dev_runtime7.jsxDEV)("path", { strokeLinecap: "round", strokeLinejoin: "round", d: "M2.25 8.25h19.5M2.25 9h19.5m-16.5 5.25h6m-6 2.25h3m-3.75 3h15a2.25 2.25 0 002.25-2.25V6.75A2.25 2.25 0 0019.5 4.5h-15a2.25 2.25 0 00-2.25 2.25v10.5A2.25 2.25 0 004.5 19.5z" }, void 0, !1, {
              fileName: "app/routes/checkout/steps/step-3.tsx",
              lineNumber: 104,
              columnNumber: 41
            }, this) }, void 0, !1, {
              fileName: "app/routes/checkout/steps/step-3.tsx",
              lineNumber: 103,
              columnNumber: 37
            }, this) }, void 0, !1, {
              fileName: "app/routes/checkout/steps/step-3.tsx",
              lineNumber: 102,
              columnNumber: 33
            }, this),
            /* @__PURE__ */ (0, import_jsx_dev_runtime7.jsxDEV)("p", { children: "VISA" }, void 0, !1, {
              fileName: "app/routes/checkout/steps/step-3.tsx",
              lineNumber: 108,
              columnNumber: 33
            }, this)
          ] }, void 0, !0, {
            fileName: "app/routes/checkout/steps/step-3.tsx",
            lineNumber: 101,
            columnNumber: 29
          }, this)
        ] }, void 0, !0, {
          fileName: "app/routes/checkout/steps/step-3.tsx",
          lineNumber: 91,
          columnNumber: 25
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime7.jsxDEV)("div", { className: "payment-details mt-5", children: [
          selected === "cash" && /* @__PURE__ */ (0, import_jsx_dev_runtime7.jsxDEV)(import_jsx_dev_runtime7.Fragment, { children: [
            /* @__PURE__ */ (0, import_jsx_dev_runtime7.jsxDEV)("div", { className: "relative", children: [
              /* @__PURE__ */ (0, import_jsx_dev_runtime7.jsxDEV)("div", { className: "absolute inset-y-0 left-0 flex items-center pl-3.5 pointer-events-none", children: /* @__PURE__ */ (0, import_jsx_dev_runtime7.jsxDEV)("svg", { xmlns: "http://www.w3.org/2000/svg", fill: "none", viewBox: "0 0 24 24", "stroke-width": "1.5", stroke: "currentColor", className: "w-6 h-6", children: /* @__PURE__ */ (0, import_jsx_dev_runtime7.jsxDEV)("path", { "stroke-linecap": "round", "stroke-linejoin": "round", d: "M12 6v12m-3-2.818l.879.659c1.171.879 3.07.879 4.242 0 1.172-.879 1.172-2.303 0-3.182C13.536 12.219 12.768 12 12 12c-.725 0-1.45-.22-2.003-.659-1.106-.879-1.106-2.303 0-3.182s2.9-.879 4.006 0l.415.33M21 12a9 9 0 11-18 0 9 9 0 0118 0z" }, void 0, !1, {
                fileName: "app/routes/checkout/steps/step-3.tsx",
                lineNumber: 118,
                columnNumber: 49
              }, this) }, void 0, !1, {
                fileName: "app/routes/checkout/steps/step-3.tsx",
                lineNumber: 117,
                columnNumber: 45
              }, this) }, void 0, !1, {
                fileName: "app/routes/checkout/steps/step-3.tsx",
                lineNumber: 116,
                columnNumber: 41
              }, this),
              /* @__PURE__ */ (0, import_jsx_dev_runtime7.jsxDEV)("input", { id: "amount", name: "amount", className: "bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-1.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500", placeholder: "\xBFCon c\xFAanto vas a abonar?", onChange: handleChangeAmount }, void 0, !1, {
                fileName: "app/routes/checkout/steps/step-3.tsx",
                lineNumber: 121,
                columnNumber: 41
              }, this)
            ] }, void 0, !0, {
              fileName: "app/routes/checkout/steps/step-3.tsx",
              lineNumber: 115,
              columnNumber: 37
            }, this),
            amount > 0 && amount > total && /* @__PURE__ */ (0, import_jsx_dev_runtime7.jsxDEV)("span", { className: "amount-exchange", children: /* @__PURE__ */ (0, import_jsx_dev_runtime7.jsxDEV)("p", { children: [
              "Tu vuelto es de $ ",
              calculateExchange(amount)
            ] }, void 0, !0, {
              fileName: "app/routes/checkout/steps/step-3.tsx",
              lineNumber: 127,
              columnNumber: 49
            }, this) }, void 0, !1, {
              fileName: "app/routes/checkout/steps/step-3.tsx",
              lineNumber: 126,
              columnNumber: 45
            }, this)
          ] }, void 0, !0, {
            fileName: "app/routes/checkout/steps/step-3.tsx",
            lineNumber: 114,
            columnNumber: 33
          }, this),
          selected === "visa" && /* @__PURE__ */ (0, import_jsx_dev_runtime7.jsxDEV)(import_jsx_dev_runtime7.Fragment, { children: [
            /* @__PURE__ */ (0, import_jsx_dev_runtime7.jsxDEV)("div", { className: "mt-5 columns-2", children: [
              /* @__PURE__ */ (0, import_jsx_dev_runtime7.jsxDEV)("input", { type: "text", id: "card", name: "card", value: cardValue, onChange: handleCardValueChange, maxLength: 16 + 3, className: "bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500", placeholder: "N\xFAmero en la tarjeta", required: !0 }, void 0, !1, {
                fileName: "app/routes/checkout/steps/step-3.tsx",
                lineNumber: 137,
                columnNumber: 41
              }, this),
              /* @__PURE__ */ (0, import_jsx_dev_runtime7.jsxDEV)("input", { type: "text", id: "vto", name: "vto", value: vtoValue, onChange: handleVtoValueChange, maxLength: 4 + 1, className: "bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-2/5 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500", placeholder: "Vencimiento", required: !0 }, void 0, !1, {
                fileName: "app/routes/checkout/steps/step-3.tsx",
                lineNumber: 138,
                columnNumber: 41
              }, this)
            ] }, void 0, !0, {
              fileName: "app/routes/checkout/steps/step-3.tsx",
              lineNumber: 136,
              columnNumber: 37
            }, this),
            /* @__PURE__ */ (0, import_jsx_dev_runtime7.jsxDEV)("div", { className: "mt-5 columns-2", children: /* @__PURE__ */ (0, import_jsx_dev_runtime7.jsxDEV)("input", { type: "text", id: "cvv", name: "cvv", maxLength: 3, className: "bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-2/5 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500", placeholder: "CVV", required: !0 }, void 0, !1, {
              fileName: "app/routes/checkout/steps/step-3.tsx",
              lineNumber: 142,
              columnNumber: 41
            }, this) }, void 0, !1, {
              fileName: "app/routes/checkout/steps/step-3.tsx",
              lineNumber: 141,
              columnNumber: 37
            }, this),
            /* @__PURE__ */ (0, import_jsx_dev_runtime7.jsxDEV)("div", { className: "mt-5 columns-2", children: /* @__PURE__ */ (0, import_jsx_dev_runtime7.jsxDEV)("input", { type: "text", id: "fullname", name: "fullname", className: "bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500", placeholder: "Nombre completo del Titular", required: !0 }, void 0, !1, {
              fileName: "app/routes/checkout/steps/step-3.tsx",
              lineNumber: 146,
              columnNumber: 41
            }, this) }, void 0, !1, {
              fileName: "app/routes/checkout/steps/step-3.tsx",
              lineNumber: 145,
              columnNumber: 37
            }, this)
          ] }, void 0, !0, {
            fileName: "app/routes/checkout/steps/step-3.tsx",
            lineNumber: 135,
            columnNumber: 33
          }, this),
          validationErrors && /* @__PURE__ */ (0, import_jsx_dev_runtime7.jsxDEV)("ul", { className: "mt-5", children: Object.values(validationErrors).map((error) => /* @__PURE__ */ (0, import_jsx_dev_runtime7.jsxDEV)("li", { className: "text-xs text-red-300", children: error }, error, !1, {
            fileName: "app/routes/checkout/steps/step-3.tsx",
            lineNumber: 156,
            columnNumber: 49
          }, this)) }, void 0, !1, {
            fileName: "app/routes/checkout/steps/step-3.tsx",
            lineNumber: 153,
            columnNumber: 37
          }, this)
        ] }, void 0, !0, {
          fileName: "app/routes/checkout/steps/step-3.tsx",
          lineNumber: 112,
          columnNumber: 25
        }, this)
      ] }, void 0, !0, {
        fileName: "app/routes/checkout/steps/step-3.tsx",
        lineNumber: 86,
        columnNumber: 21
      }, this) }, void 0, !1, {
        fileName: "app/routes/checkout/steps/step-3.tsx",
        lineNumber: 85,
        columnNumber: 17
      }, this),
      /* @__PURE__ */ (0, import_jsx_dev_runtime7.jsxDEV)("div", { className: "payment-amount", children: [
        /* @__PURE__ */ (0, import_jsx_dev_runtime7.jsxDEV)("span", { children: /* @__PURE__ */ (0, import_jsx_dev_runtime7.jsxDEV)("p", { children: "Total" }, void 0, !1, {
          fileName: "app/routes/checkout/steps/step-3.tsx",
          lineNumber: 168,
          columnNumber: 25
        }, this) }, void 0, !1, {
          fileName: "app/routes/checkout/steps/step-3.tsx",
          lineNumber: 167,
          columnNumber: 21
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime7.jsxDEV)("div", { className: "separator mt-2" }, void 0, !1, {
          fileName: "app/routes/checkout/steps/step-3.tsx",
          lineNumber: 171,
          columnNumber: 21
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime7.jsxDEV)("div", { className: "amount-info", children: /* @__PURE__ */ (0, import_jsx_dev_runtime7.jsxDEV)("span", { children: /* @__PURE__ */ (0, import_jsx_dev_runtime7.jsxDEV)("p", { children: "$ 3.500" }, void 0, !1, {
          fileName: "app/routes/checkout/steps/step-3.tsx",
          lineNumber: 175,
          columnNumber: 29
        }, this) }, void 0, !1, {
          fileName: "app/routes/checkout/steps/step-3.tsx",
          lineNumber: 174,
          columnNumber: 25
        }, this) }, void 0, !1, {
          fileName: "app/routes/checkout/steps/step-3.tsx",
          lineNumber: 173,
          columnNumber: 21
        }, this)
      ] }, void 0, !0, {
        fileName: "app/routes/checkout/steps/step-3.tsx",
        lineNumber: 166,
        columnNumber: 17
      }, this)
    ] }, void 0, !0, {
      fileName: "app/routes/checkout/steps/step-3.tsx",
      lineNumber: 84,
      columnNumber: 13
    }, this),
    /* @__PURE__ */ (0, import_jsx_dev_runtime7.jsxDEV)("div", { className: "mt-16 flex justify-between", children: [
      /* @__PURE__ */ (0, import_jsx_dev_runtime7.jsxDEV)("div", { className: "mt-16 mx-3", children: /* @__PURE__ */ (0, import_jsx_dev_runtime7.jsxDEV)("button", { className: "cta sm", onClick: handlePrev, children: "Anterior" }, void 0, !1, {
        fileName: "app/routes/checkout/steps/step-3.tsx",
        lineNumber: 183,
        columnNumber: 21
      }, this) }, void 0, !1, {
        fileName: "app/routes/checkout/steps/step-3.tsx",
        lineNumber: 182,
        columnNumber: 17
      }, this),
      /* @__PURE__ */ (0, import_jsx_dev_runtime7.jsxDEV)("div", { className: "mt-16 mx-3", children: /* @__PURE__ */ (0, import_jsx_dev_runtime7.jsxDEV)("button", { className: "cta sm confirm", children: "Confirmar Pedido" }, void 0, !1, {
        fileName: "app/routes/checkout/steps/step-3.tsx",
        lineNumber: 191,
        columnNumber: 21
      }, this) }, void 0, !1, {
        fileName: "app/routes/checkout/steps/step-3.tsx",
        lineNumber: 190,
        columnNumber: 17
      }, this)
    ] }, void 0, !0, {
      fileName: "app/routes/checkout/steps/step-3.tsx",
      lineNumber: 181,
      columnNumber: 13
    }, this)
  ] }, void 0, !0, {
    fileName: "app/routes/checkout/steps/step-3.tsx",
    lineNumber: 83,
    columnNumber: 9
  }, this);
}

// app/routes/success.tsx
var success_exports = {};
__export(success_exports, {
  default: () => SuccessPage,
  links: () => links3
});

// app/styles/success.css
var success_default = "/build/_assets/success-4MB6XVQ3.css";

// app/routes/success.tsx
var import_jsx_dev_runtime8 = require("react/jsx-dev-runtime"), links3 = () => [
  { rel: "stylesheet", href: success_default }
];
function SuccessPage() {
  return /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("div", { className: "container confirmed", children: /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("div", { className: "confirmed-card", children: [
    /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("span", { children: /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("h1", { children: "\xA1Pedido Confirmado!" }, void 0, !1, {
      fileName: "app/routes/success.tsx",
      lineNumber: 13,
      columnNumber: 15
    }, this) }, void 0, !1, {
      fileName: "app/routes/success.tsx",
      lineNumber: 13,
      columnNumber: 9
    }, this),
    /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("div", { className: "mt-10 mx-3", children: /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("button", { disabled: !0, className: "cta sm confirm", children: "Estado del pedido" }, void 0, !1, {
      fileName: "app/routes/success.tsx",
      lineNumber: 16,
      columnNumber: 11
    }, this) }, void 0, !1, {
      fileName: "app/routes/success.tsx",
      lineNumber: 15,
      columnNumber: 9
    }, this)
  ] }, void 0, !0, {
    fileName: "app/routes/success.tsx",
    lineNumber: 12,
    columnNumber: 7
  }, this) }, void 0, !1, {
    fileName: "app/routes/success.tsx",
    lineNumber: 11,
    columnNumber: 5
  }, this);
}

// app/routes/index.tsx
var routes_exports = {};
__export(routes_exports, {
  default: () => Index,
  meta: () => meta2
});
var import_react12 = require("@remix-run/react"), import_jsx_dev_runtime9 = require("react/jsx-dev-runtime"), meta2 = () => ({
  title: "\xBFQue est\xE1s buscando? | Comercio 1"
});
function Index() {
  return /* @__PURE__ */ (0, import_jsx_dev_runtime9.jsxDEV)("main", { children: /* @__PURE__ */ (0, import_jsx_dev_runtime9.jsxDEV)(import_react12.Link, { to: "checkout", children: /* @__PURE__ */ (0, import_jsx_dev_runtime9.jsxDEV)("button", { children: "Ver pedido" }, void 0, !1, {
    fileName: "app/routes/index.tsx",
    lineNumber: 14,
    columnNumber: 9
  }, this) }, void 0, !1, {
    fileName: "app/routes/index.tsx",
    lineNumber: 13,
    columnNumber: 7
  }, this) }, void 0, !1, {
    fileName: "app/routes/index.tsx",
    lineNumber: 12,
    columnNumber: 5
  }, this);
}

// server-assets-manifest:@remix-run/dev/assets-manifest
var assets_manifest_default = { entry: { module: "/build/entry.client-BLFEE6NL.js", imports: ["/build/_shared/chunk-LQ53JVGW.js", "/build/_shared/chunk-JEH7KJKD.js", "/build/_shared/chunk-FCC3XGIV.js"] }, routes: { root: { id: "root", parentId: void 0, path: "", index: void 0, caseSensitive: void 0, module: "/build/root-WPZB6JDX.js", imports: void 0, hasAction: !1, hasLoader: !1, hasCatchBoundary: !1, hasErrorBoundary: !1 }, "routes/checkout": { id: "routes/checkout", parentId: "root", path: "checkout", index: void 0, caseSensitive: void 0, module: "/build/routes/checkout-6XX7ZU7I.js", imports: void 0, hasAction: !1, hasLoader: !0, hasCatchBoundary: !1, hasErrorBoundary: !1 }, "routes/checkout/steps": { id: "routes/checkout/steps", parentId: "routes/checkout", path: "steps", index: void 0, caseSensitive: void 0, module: "/build/routes/checkout/steps-5U236RZE.js", imports: void 0, hasAction: !1, hasLoader: !1, hasCatchBoundary: !1, hasErrorBoundary: !1 }, "routes/checkout/steps/step-1": { id: "routes/checkout/steps/step-1", parentId: "routes/checkout/steps", path: "step-1", index: void 0, caseSensitive: void 0, module: "/build/routes/checkout/steps/step-1-SYT4NS4L.js", imports: ["/build/_shared/chunk-P3MTAGQC.js"], hasAction: !0, hasLoader: !1, hasCatchBoundary: !1, hasErrorBoundary: !1 }, "routes/checkout/steps/step-2": { id: "routes/checkout/steps/step-2", parentId: "routes/checkout/steps", path: "step-2", index: void 0, caseSensitive: void 0, module: "/build/routes/checkout/steps/step-2-EXXOORPM.js", imports: ["/build/_shared/chunk-P3MTAGQC.js"], hasAction: !0, hasLoader: !1, hasCatchBoundary: !1, hasErrorBoundary: !1 }, "routes/checkout/steps/step-3": { id: "routes/checkout/steps/step-3", parentId: "routes/checkout/steps", path: "step-3", index: void 0, caseSensitive: void 0, module: "/build/routes/checkout/steps/step-3-WPXDBIWS.js", imports: ["/build/_shared/chunk-P3MTAGQC.js"], hasAction: !0, hasLoader: !1, hasCatchBoundary: !1, hasErrorBoundary: !1 }, "routes/index": { id: "routes/index", parentId: "root", path: void 0, index: !0, caseSensitive: void 0, module: "/build/routes/index-YWD3QGDR.js", imports: void 0, hasAction: !1, hasLoader: !1, hasCatchBoundary: !1, hasErrorBoundary: !1 }, "routes/success": { id: "routes/success", parentId: "root", path: "success", index: void 0, caseSensitive: void 0, module: "/build/routes/success-XUAAK7UR.js", imports: void 0, hasAction: !1, hasLoader: !1, hasCatchBoundary: !1, hasErrorBoundary: !1 } }, version: "52bca398", hmr: void 0, url: "/build/manifest-52BCA398.js" };

// server-entry-module:@remix-run/dev/server-build
var assetsBuildDirectory = "public/build", future = { v2_dev: !1, unstable_postcss: !1, unstable_tailwind: !1, v2_errorBoundary: !1, v2_headers: !1, v2_meta: !1, v2_normalizeFormMethod: !1, v2_routeConvention: !1 }, publicPath = "/build/", entry = { module: entry_server_exports }, routes = {
  root: {
    id: "root",
    parentId: void 0,
    path: "",
    index: void 0,
    caseSensitive: void 0,
    module: root_exports
  },
  "routes/checkout": {
    id: "routes/checkout",
    parentId: "root",
    path: "checkout",
    index: void 0,
    caseSensitive: void 0,
    module: checkout_exports
  },
  "routes/checkout/steps": {
    id: "routes/checkout/steps",
    parentId: "routes/checkout",
    path: "steps",
    index: void 0,
    caseSensitive: void 0,
    module: steps_exports
  },
  "routes/checkout/steps/step-1": {
    id: "routes/checkout/steps/step-1",
    parentId: "routes/checkout/steps",
    path: "step-1",
    index: void 0,
    caseSensitive: void 0,
    module: step_1_exports
  },
  "routes/checkout/steps/step-2": {
    id: "routes/checkout/steps/step-2",
    parentId: "routes/checkout/steps",
    path: "step-2",
    index: void 0,
    caseSensitive: void 0,
    module: step_2_exports
  },
  "routes/checkout/steps/step-3": {
    id: "routes/checkout/steps/step-3",
    parentId: "routes/checkout/steps",
    path: "step-3",
    index: void 0,
    caseSensitive: void 0,
    module: step_3_exports
  },
  "routes/success": {
    id: "routes/success",
    parentId: "root",
    path: "success",
    index: void 0,
    caseSensitive: void 0,
    module: success_exports
  },
  "routes/index": {
    id: "routes/index",
    parentId: "root",
    path: void 0,
    index: !0,
    caseSensitive: void 0,
    module: routes_exports
  }
};
// Annotate the CommonJS export names for ESM import in node:
0 && (module.exports = {
  assets,
  assetsBuildDirectory,
  entry,
  future,
  publicPath,
  routes
});
/*! Bundled license information:

@remix-run/css-bundle/dist/index.js:
  (**
   * @remix-run/css-bundle v1.19.3
   *
   * Copyright (c) Remix Software Inc.
   *
   * This source code is licensed under the MIT license found in the
   * LICENSE.md file in the root directory of this source tree.
   *
   * @license MIT
   *)
*/
//# sourceMappingURL=index.js.map
